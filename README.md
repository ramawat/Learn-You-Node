# Learn-You-Node
Implementation for the 13 Exercises. It's a great module to get a head start in Learning Node.

To install node and get it up and running please go to [Node Website](https://nodejs.org/).
After installing node, to get started with the LearnYouNode module, go to [Learn You Node Github Repo](https://github.com/workshopper/learnyounode)

To run the solution for any exercise, uncomment that code block (and comment the rest !! :D) and save.
Then inside terminal, type
```
learnyounode verify /path/to/program.js
```

* For Question#6, you would need to uncomment the <code>module.js</code> file as well.

For any questions, start an issue here or reach out to me at [@ramawat](https://twitter.com/ramawat)
