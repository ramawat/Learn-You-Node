/*----------------------*/

/**	Ex13
		HTTP JSON API Server
**/

var http = require('http'),
		url = require('url'),
		server = undefined;

server = http.createServer(function (request, response){
	if(request.method == 'GET'){
		parsedUrl = url.parse(request.url,true);
		response.writeHead(200,{"Content-Type": "application/json"});
		date = new Date(parsedUrl.query.iso);
		if(parsedUrl.pathname == "/api/parsetime"){
			data = { 
				hour: date.getHours(),
				minute: date.getMinutes(),
				second: date.getSeconds() 
			};
		}
		if(parsedUrl.pathname == "/api/unixtime"){
			data = {
				unixtime: (new Date(parsedUrl.query.iso)).getTime()
			};
		}
		response.end(JSON.stringify(data));
	}
	else{
		return request.end("Not GET\n");
	}
});

server.listen(parseInt(process.argv[2]));

/*----------------------*/

/**	Ex12
		HTTP UpperCaser of received POST requests
**/

/*var http = require('http'),
		map = require('through2-map'),
		server = undefined;

server = http.createServer(function (request,response){
	if (request.method == 'POST'){
		request.pipe(map(function (chunk){
			return chunk.toString().toUpperCase();
		})).pipe(response);
	}
	else{
		return request.end("Not POST\n");
	}
});

server.listen(parseInt(process.argv[2]));*/

/*----------------------*/

/**	Ex11
		HTTP File Server	
**/

/*var http = require('http'),
		fs = require('fs'),
		server = undefined;

server = http.createServer(function (request, response){
	request = fs.createReadStream(process.argv[3]);
	request.pipe(response);
});
server.listen(parseInt(process.argv[2]));*/

/*----------------------*/

/**	Ex10
		TCP Time Server
**/

/*var net = require('net'),
		format = require('strftime'),
		server = undefined;

server = net.createServer(function(socket){
	var date = new Date();
	var data = format('%F %R',date);
	socket.end(data);
});
server.listen(parseInt(process.argv[2]));*/

/*----------------------*/

/**	Ex9
		Juggling Async
**/

/* Without Nesting Callbacks
-------------------
var http = require('http'),
		bl = require('bl'),
		results = [],
		counter = 0;

function printResults(){
	for (var i = 0; i < 3; i++) {
		console.log(results[i]);
	}
}	

function responseEvent(count){
	http.get(process.argv[2+count], function after(response){
		response.setEncoding("utf-8");
		response.pipe(bl(function(err,data){
			results[count] = data.toString();
			counter++;
			if(counter == 3){printResults();}
		}));
	});
}

responseEvent(0);
responseEvent(1);
responseEvent(2);*/

/* Nesting Callbacks
-------------------
var http = require('http'),
		bl = require('bl');

function responseEvent(callback){
	http.get(process.argv[2], function firstResp(first){
		first.setEncoding("utf-8");
		first.pipe(bl(function(err,data){
			callback(data.toString());
			http.get(process.argv[3], function secondResp(second){
				second.setEncoding("utf-8");
				second.pipe(bl(function(err,data){
					callback(data.toString());
					http.get(process.argv[4], function thirdResp(third){
						third.setEncoding("utf-8");
						third.pipe(bl(function(err,data){
							callback(data.toString());
						}));
					});
				}));
			});
		}));
	});
}

function display(param){
	console.log(param);
}

responseEvent(display);*/

/*----------------------*/

/**	Ex8
		Collecting HTTP data
**/

/*var http = require('http'),
		bl = require('bl');

function responseEvent(callback){
	http.get(process.argv[2], function afterGet(response){
		response.setEncoding("utf-8");
		response.pipe(bl(function (err,data){
			if(err){
				callback(err);
			}
			callback(data.toString().length);
			callback(data.toString());
		}));
	});
}

function display(param){
	console.log(param);
}

responseEvent(display);*/

/*----------------------*/

/**	Ex7
		Perform HTTP GET request
**/

/*var http = require('http');

function responseEvent(callback){
	http.get(process.argv[2], function afterGet(response){
		response.setEncoding("utf-8");
		response.on('data', function(data){
			callback(data);
		});
		response.on('error', function(error){
			callback(error);
		});
	});
}

function display(param){
	console.log(param);
}

responseEvent(display);*/

/*----------------------*/

/**	Ex6
		Creating own modules
**/

/*var mymodule = require('./module');

function showListing(callback){
	mymodule(process.argv[2],process.argv[3],function after(err,files){
		files.forEach(function(file){
			callback(file);
		});
	});
}

function display(param){
	console.log(param);
}

showListing(display);*/

/*----------------------*/

/**	Ex5
		Print List of Files with a given extension in a given directory
**/

/*var fs = require('fs'),
		path = require('path');		

function showListing(callback){
	fs.readdir(process.argv[2], function afterRead(err, files){
		files.forEach(function(name){
			var ext = '.'.concat(process.argv[3]);
			if(path.extname(name)===ext){
				callback(name);
			}
		});
	});
}

function dispListing(param){
	console.log(param);
}

showListing(dispListing);*/

/*----------------------*/

/**	Ex4
		Using single asynchronus filesystem (fs module)
**/

/*var fs = require('fs'),
		counter = undefined;

function countLines(callback){
	fs.readFile(process.argv[2], function afterRead(err,str){
		counter = str.toString().split("\n").length - 1;
		callback();
	});
}

function countNumber(){
	console.log(counter);
}

countLines(countNumber);*/

/*----------------------*/

/** Ex3 
		Using single scynchronous filesystem (fs module)
**/

/*var fs = require('fs'),
	  buf = fs.readFileSync(process.argv[2]),
	 	str = buf.toString();

function countLines(){
	var subs = str.split("\n");
	return subs.length-1;
}

console.log(countLines());*/


/*----------------------*/

/** Ex 2  
		Command line arguments
**/
/*function doSum(){
	var sum = 0;
	for(var i=2; i <process.argv.length; i++ ){
	sum += Number(process.argv[i]);
	}	
	return sum;
}
console.log(doSum());*/

/*----------------------*/

/**  Ex 1  
		 Getting Started
**/
/*console.log("HELLO WORLD");*/
